//
//  User.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 07.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import Foundation

public class User: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }
    
    @NSManaged public var id: Int64
    @NSManaged public var email: String
    @NSManaged public var name: String
    @NSManaged public var phone: String
    @NSManaged public var username: String
    @NSManaged public var website: String
    @NSManaged public var address: Address
    @NSManaged public var company: Company
    @NSManaged public var albums: Set<Album>
    @NSManaged public var posts: Set<Post>
}
