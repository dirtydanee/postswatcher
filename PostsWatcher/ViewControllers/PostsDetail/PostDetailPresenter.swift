//
//  PostDetailPresenter.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 04.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class PostDetailPresenter: Presenter {
    
    var viewController: UIViewController {
        return self.navigationController
    }
    
    var parent: Presenter?
    
    fileprivate let postDetailViewController: PostDetailViewController
    private let navigationController: UINavigationController
    private var dataSource: PostDetailDataSource?
    
    init() {
        self.postDetailViewController = PostDetailViewController()
        self.navigationController = UINavigationController(rootViewController: self.postDetailViewController)
    }
    
    func notify(event: Event) {
        switch event {
        case .didSelect(postViewModel: let viewModel):
            let dataSource = PostDetailDataSource(viewModel: viewModel)
            dataSource.delegate = self
            self.postDetailViewController.dataSource = dataSource
            self.viewController.view.isHidden = false
            self.postDetailViewController.tableView.dataSource = dataSource
            self.postDetailViewController.tableView.delegate = dataSource
            self.postDetailViewController.tableView.reloadData()
            self.dataSource = dataSource
        default:
            break
        }
    }
}   

// MARK: - PostDetailDataSourceDelegate

extension PostDetailPresenter: PostDetailDataSourceDelegate {
    func postDetailDataSource(_ dataSource: PostDetailDataSource, sectionNeedsUpdatingAt section: Int) {
        self.postDetailViewController.tableView.reloadData()
    }
}
