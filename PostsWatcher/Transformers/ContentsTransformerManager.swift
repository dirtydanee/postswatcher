//
//  TransformerManager.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import SwiftyJSON
import CoreData

/// Unified interface of all applications content transformers
protocol ContentTransformer {
    
    /// Identifier of the transformer
    var identifier: ContentIdentifier { get }
    
    /// Transform JSON into usable application content
    func transform(json: JSON) throws
}

final class ContentsTransformerManager {
    
    typealias ContetsTransformerManagerCompletionHandler = (_ success: Bool) -> Void
    
    enum TransformerError: Error {
        case unknownDataWithIdentifier(identifier: String)
        case invalidStructure(identifier: String)
        case invalidEntity(identifier: String)
    }
    
    let managedContext: NSManagedObjectContext
    private let transformers: [ContentTransformer]
    private let transformerQueue: OperationQueue
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        
        self.transformerQueue = OperationQueue()
        transformerQueue.maxConcurrentOperationCount = 1
        
        let userTransformer = try UserTransformer(managedContext: managedContext)
        let postTransformer = try PostTransformer(managedContext: managedContext)
        let albumTransformer = try AlbumTransformer(managedContext: managedContext)
        let photoTransformer = try PhotoTransformer(managedContext: managedContext)
        
        self.transformers = [userTransformer, postTransformer, albumTransformer, photoTransformer]
    }
    
    // MARK: - Public API
     
    func transform(contentDatas: [ContentData], completion: @escaping ContetsTransformerManagerCompletionHandler) throws {
        
        var identifiedTransformOperations: [ContentIdentifier: TransformOperation] = [:]
        
        // Create operations
        try contentDatas.forEach { (contentData) in
            guard let transformer = transformers.first(where: { $0.identifier == contentData.identifier }) else {
                throw TransformerError.unknownDataWithIdentifier(identifier: contentData.identifier.rawValue)
            }
            
            let json = JSON(data: contentData.data)
            let transformOperation = TransformOperation(contentTransformer: transformer, json: json)
            identifiedTransformOperations[contentData.identifier] = transformOperation 
        }
        
        // Create save operation    
        let saveOperation = SaveOperation(managedContext: self.managedContext)
        saveOperation.completionBlock = {
            completion(true)
        }
        
        // Set dependecies
        try identifiedTransformOperations.throwingValue(for: .albums).addDependency(identifiedTransformOperations.throwingValue(for: .photos))
        try identifiedTransformOperations.throwingValue(for: .posts).addDependency(identifiedTransformOperations.throwingValue(for: .albums))
        try identifiedTransformOperations.throwingValue(for: .users).addDependency(identifiedTransformOperations.throwingValue(for: .posts))
        try saveOperation.addDependency(identifiedTransformOperations.throwingValue(for: .users))
        
        // Create all exectuable operations
        var operations: [Operation] = identifiedTransformOperations.valuesArray
        operations.append(saveOperation)
        
        // Start transforming
        self.transformerQueue.addOperations(operations, waitUntilFinished: false)
    }
}
