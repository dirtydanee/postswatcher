//
//  PostsPresenter.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 04.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class PostsPresenter: Presenter {
    
    var viewController: UIViewController {
        return self.navigationController
    }
    
    var parent: Presenter?
    fileprivate let postsViewController: PostsViewController
    private let navigationController: UINavigationController
    private var dataSource: PostsDataSource?
    
    init() {
        self.postsViewController = PostsViewController()
        self.navigationController = UINavigationController(rootViewController: self.postsViewController)
    }
    
    func notify(event: Event) {
        switch event {
        case .postDataSourceBecameAvailable(dataSource: let dataSource):
            dataSource.delegate = self
            dataSource.reload()
            self.postsViewController.tableView.dataSource = dataSource
            self.postsViewController.tableView.delegate = dataSource
            self.postsViewController.searchController.searchResultsUpdater = dataSource
            self.postsViewController.tableView.reloadData()
            self.dataSource = dataSource
        default:
            break
        }
    }
} 

// MARK: - PostsDataSourceDelegate

extension PostsPresenter: PostsDataSourceDelegate {
    
    func dataSource(_ dataSource: PostsDataSource, didSelect postViewModel: PostViewModel) {
        let event = Event.didSelect(postViewModel: postViewModel)
        self.parent?.notify(event: event)
    }
    
    func dataSource(_ dataSource: PostsDataSource, didDelete postViewModel: PostViewModel) {
        let event = Event.didDelete(postViewModel: postViewModel)
        self.parent?.notify(event: event)
    }
    
    func dataSourceDidRequestReload(_ dataSource: PostsDataSource) {
        self.postsViewController.tableView.reloadData()
    }
}
