//
//  AlbumStoreTests.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 08.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import CoreData
@testable import PostsWatcher

class AlbumsStoreTests: CoreDataTestCase {
    
    var store: AlbumsStore!
    
    override func setUp() {
        super.setUp()
        self.store = try? AlbumsStore(managedContext: self.managedContext)
    }
    
    override func tearDown() {
        self.store = nil
        super.tearDown()
    }
    
    func testfetchAlbumWithExistingIdentifier() throws {
        let identifier = Int64(1000)
        try createEntity(with: identifier)
        XCTAssertNotNil(try self.store.album(with: identifier))
    }
    
    func testfetchAlbumWithNonExistingIdentifier() throws {
        XCTAssertNil(try self.store.album(with: Int64(-1)))
    }
    
    func testAlbumsForExistingUser() throws {
        
        let identifier = Int64(1)
        let album = try createEntity(with: identifier)
        album.userId = identifier
        
        let userStore: UsersStore = try UsersStore(managedContext: self.managedContext)
        let _: User = try userStore.fetchOrCreateEntity(with: identifier)
        let albums = try self.store.albums(forUser: identifier)
        XCTAssertEqual(albums.count, 1)
        XCTAssertTrue(albums.contains(where: {$0.userId == identifier}))
    }
    
    func testAlbumsForNonExistingUser() throws {
        let albums = try self.store.albums(forUser: Int64(-1))
        XCTAssertTrue(albums.isEmpty)
    }
    
    @discardableResult
    private func createEntity(with identifier: Int64) throws -> Album {
        guard let album = NSManagedObject(entity: self.store.entity, insertInto: self.managedContext) as? Album else {
            throw CoreDataTestError.unableToCreateEntity(identifier: self.store.contentIdentifier.rawValue)
        }
        album.id = identifier
        return album
    }  
}
