//
//  PhotoViewModel.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 05.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation

struct PhotoViewModel {
    
    var title: String {
        return self.photo.title
    }
    
    var url: URL? {
        return URL(string: self.photo.url) 
    }
    
    let photo: Photo
    
    init(photo: Photo) {
        self.photo = photo
    }
}
