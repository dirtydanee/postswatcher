//
//  ContentData.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation

// Wrapper on Data for convinient identification  
struct ContentData {
    
    /// The identifier of the data
    let identifier: ContentIdentifier
    
    /// The raw content data
    let data: Data
}
