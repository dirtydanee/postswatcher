//
//  PostsViewController.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class PostsViewController: UIViewController {
    
    var tableView: UITableView!
    var searchController: UISearchController!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
       
    // MARK: ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Challenge accepted"
        self.setupTableView()
        self.setupSearchContoller()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.frame.size = self.view.frame.size
    }

    // MARK: Private API
    
    private func setupTableView() {
        let tableView = UITableView(frame: CGRect(origin: .zero, size: .zero), style: .plain)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: PostTableViewCell.identifier)
        self.view.addSubview(tableView)
        self.tableView = tableView
    }
    
    private func setupSearchContoller() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        self.searchController = searchController
        self.tableView.tableHeaderView = searchController.searchBar
    }
}
