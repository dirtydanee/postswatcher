//
//  ContentRequestFactory.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import Alamofire

final class ContentRequestFactory {
    
    enum ContentRequestFactoryError: Error {
        case unableToCreateURL(forPath: String)
    }
    
    private struct Constants {
        static let basePath = "http://jsonplaceholder.typicode.com/"
    }
    
    static func makeContentDataRequest(forIdentifier identifier: ContentIdentifier) throws -> ContentDataRequest {
        let request = try makeRequest(for: "\(Constants.basePath)\(identifier.rawValue)")
        return ContentDataRequest(identifier: identifier, dataRequest: request)
    }
    
    private static func makeRequest(for path: String) throws -> DataRequest {
        guard let url = URL(string: path) else {
            throw ContentRequestFactoryError.unableToCreateURL(forPath: path)
        }      
        
        return Alamofire.request(url) 
    } 
}
