//
//  PhotoTableViewCell.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 06.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit
import AlamofireImage

final class PhotoTableViewCell: UITableViewCell {

    static let identifier: String = String(describing: PhotoTableViewCell.self)
    @IBOutlet private weak var photoImageView: UIImageView!
    
    func configure(viewModel: PhotoViewModel) {
        guard let url = viewModel.url else { 
            clog("Missing url for viewModel: \(viewModel)", priority: .error)
            return 
        }
        self.photoImageView.af_setImage(withURL: url)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoImageView.image = nil
    }
}
