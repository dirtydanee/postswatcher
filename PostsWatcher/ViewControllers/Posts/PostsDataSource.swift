//
//  PostsDataSource.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit
import CoreData 

protocol PostsDataSourceDelegate: class {
    
    /// The dataSource signaling if a post has been selected by the user
    ///
    /// - Parameters:
    ///   - dataSource: The dataSource holding the post
    ///   - postViewModel: The viewModel of the selected post
    func dataSource(_ dataSource: PostsDataSource, didSelect postViewModel: PostViewModel)
    
    /// The dataSource signaling if a post has been deleted by the user
    ///
    /// - Parameters:
    ///   - dataSource: The dataSource holding the post
    ///   - postViewModel: The viewModel of the deleted post
    func dataSource(_ dataSource: PostsDataSource, didDelete postViewModel: PostViewModel)
    
    /// The dataSource signaling for its tableView to be reloaded
    ///
    /// - Parameter dataSource: The dataSource holding the posts to be reloaded
    func dataSourceDidRequestReload(_ dataSource: PostsDataSource)
}

final class PostsDataSource: NSObject {
    
    let managedContext: NSManagedObjectContext
    fileprivate let postsStore: PostsStore
    fileprivate var posts: [Post] = []
    
    weak var delegate: PostsDataSourceDelegate?
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        self.postsStore = try PostsStore(managedContext: managedContext)
    }
    
    // MARK: Public API
    
    func reload() {
        do {
            self.posts = try Array(self.postsStore.allPosts())
        } catch let e {
            clog("Error while reloading posts. Error: \(e)", priority: .error)
        }
    }
    
    // MARK: Private API
    
    fileprivate func search(`for` text: String) throws {
        if text.isEmpty {
            self.reload()
        } else {
            self.posts = try Array(self.postsStore.posts(containing: text))
        }
        self.delegate?.dataSourceDidRequestReload(self)
    }
}

// MARK: UITableViewDelegate

extension PostsDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            do {
                let deletablePost = self.posts[indexPath.row]
                self.posts.remove(at: indexPath.row)
                
                try self.postsStore.delete(deletablePost)
                
                tableView.deleteRows(at: [indexPath], with: .fade)
                let viewModel = PostViewModel(post: deletablePost)
                self.delegate?.dataSource(self, didDelete: viewModel)
            } catch let e {
                clog("Unable to save context after deleting post. ERROR: \(e)", priority: .error)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = self.posts[indexPath.row]
        let viewModel = PostViewModel(post: post)
        self.delegate?.dataSource(self, didSelect: viewModel)
    }
}

// MARK: UITableViewDataSource

extension PostsDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = self.posts[indexPath.row]
        let viewModel = PostViewModel(post: post)
        guard let postCell = tableView.dequeueReusableCell(withIdentifier: PostTableViewCell.identifier, for: indexPath) as? PostTableViewCell else {
            clog("Unable to typecast reusable PostTableViewCell in PostsDataSource.", priority: .error)
            fatalError()
        }
        postCell.configure(with: viewModel)
        return postCell
    }
}

// MARK: UISearchResultsUpdating

extension PostsDataSource: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            clog("Missing text in PostsDataSource on UISearchController. Unable to carry out search.", priority: .error)
            return
        }
        
        do {
            try self.search(for: searchText)
        } catch let e {
            clog("Error while searching posts. ERROR: \(e.localizedDescription)", priority: .error)
        }
    }
}

