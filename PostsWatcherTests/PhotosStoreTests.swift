//
//  PhotosStoreTests.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 08.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import CoreData
@testable import PostsWatcher

class PhotosStoreTests: CoreDataTestCase {
    
    var store: PhotosStore!
    
    override func setUp() {
        super.setUp()
        self.store = try? PhotosStore(managedContext: self.managedContext)
    }
    
    override func tearDown() {
        self.store = nil
        super.tearDown()
    }
    
    func testfetchPhotoWithExistingIdentifier() throws {
        let identifier = Int64(1000)
        try createEntity(with: identifier)
        XCTAssertNotNil(try self.store.photo(with: identifier))
    }
    
    func testfetchPhotoWithNonExistingIdentifier() throws {
        XCTAssertNil(try self.store.photo(with: Int64(-1)))
    }
    
    func testPhotosForExistingAlbum() throws {
        
        let photoOne = try createEntity(with: Int64(1))
        let photoTwo = try createEntity(with: Int64(2))
        
        let albumId = Int64(1)
        photoOne.albumId = albumId
        photoTwo.albumId = albumId
        
        let albumStore: AlbumsStore = try AlbumsStore(managedContext: self.managedContext)
        
        let album: Album = try albumStore.fetchOrCreateEntity(with: albumId)
        album.photos = [photoOne, photoTwo]
        let photos = try self.store.photos(forAlbum: albumId)
        
        XCTAssertEqual(photos.count, 2)
        XCTAssertTrue(photos.contains(photoOne))
        XCTAssertTrue(photos.contains(photoTwo))
    }
    
    func testPhotosForNonExistingAlbum() throws {
        let photos = try self.store.photos(forAlbum: Int64(-1))
        XCTAssertTrue(photos.isEmpty)
    }
    
    @discardableResult
    private func createEntity(with identifier: Int64) throws -> Photo {
        guard let photo = NSManagedObject(entity: self.store.entity, insertInto: self.managedContext) as? Photo else {
            throw CoreDataTestError.unableToCreateEntity(identifier: self.store.contentIdentifier.rawValue)
        }
        photo.id = identifier
        return photo
    }  
}
