//
//  ApplicationPresenter.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class ApplicationPresenter: Presenter {
    
    var viewController: UIViewController {
        return self.postsWatcherPresenter.viewController
    }
    
    var parent: Presenter? = nil
    
    private let coreDataStack: CoreDataStack
    private let contentSynchronizer: ContentSynchronizer
    private let postsWatcherPresenter: PostsWatcherPresenter
    
    init() throws {
        self.coreDataStack = CoreDataStack()
        self.contentSynchronizer = try ContentSynchronizer(coreDataStack: self.coreDataStack)
        self.postsWatcherPresenter = PostsWatcherPresenter()
        self.postsWatcherPresenter.parent = self
    }
    
    func startUserInterface() {
        self.postsWatcherPresenter.state = .loading
    }
    
    func synronizeApplicationContent() {
        self.contentSynchronizer.startSyncing { [weak self] (success) in
            guard let `self` = self else { 
                clog("ApplicationSupervisor deallocated while synronizing content.", priority: .error)
                return 
            }
            
            DispatchQueue.main.async {
                self.postsWatcherPresenter.state = .ready
                if success {
                    clog("Syncmanager has finished with success, presenting user interface.", priority: .general)
                    do {
                        let postsDataSource = try PostsDataSource(managedContext: self.coreDataStack.persistentContainer.viewContext)
                        let event = Event.postDataSourceBecameAvailable(dataSource: postsDataSource)
                        self.postsWatcherPresenter.notify(event: event)
                    } catch let e {
                        clog("ApplicationSupervisor failed while setting up userInterface. Error: \(e.localizedDescription)", priority: .error)
                    }
                } else {
                    clog("syncmanager has failed", priority: .error)
                }
            }
        }
    }
    
    func saveCoreDataContext() {
        self.coreDataStack.saveContext()
    }
    
    func notify(event: Event) {}
}
