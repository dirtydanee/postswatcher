//
//  Presenter.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 06.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

/// Presenters are encapsuling the actions in the application`s lifecycle aligned in a tree structure
/// Presenters are responsible for: 
///  - presenting user interface
///  - handling changes on the user interface
///  - communicating with other Presenters
protocol Presenter: class {
    
    /// The presented viewController
    var viewController: UIViewController { get }
    
    /// The parent Presenter of a given presenter
    var parent: Presenter? { get set }
    
    /// Handling Events that are routed through presenters
    func notify(event: Event)
}

/// Record of an important change in the applications life-cycle or a user action
/// - postDataSourceBecameAvailable: The dataSource of the application has been created
/// - didSelect: User has selected a Post
/// - didDelete: User has deleted a Post
enum Event {
    case postDataSourceBecameAvailable(dataSource: PostsDataSource)
    case didSelect(postViewModel: PostViewModel)
    case didDelete(postViewModel: PostViewModel)
} 
