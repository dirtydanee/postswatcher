//
//  AddressTransformer.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import SwiftyJSON

final class AddressTransformer {
    
    private struct Constants {
        static let entityName = "Address"
        static let defaultSortDescriptorKey = "zipcode"
    }
    
    private let entity: NSEntityDescription
    private let fetchedResultsController: NSFetchedResultsController<Address>
    let managedContext: NSManagedObjectContext
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        
        let fetchRequest: NSFetchRequest<Address> = Address.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: Constants.defaultSortDescriptorKey, ascending: true)]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        
        guard let entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: self.managedContext) else {
            throw CoreDataStack.CoreDataError.entitiyCreationFailed(identifier: Constants.entityName)
        }
        self.entity = entity
    }
    
    // MARK: - Public API
    
    func transformSubEntity(json: JSON) throws -> Address {
        let latitude = json["geo"]["lat"].stringValue
        let longitude = json["geo"]["lng"].stringValue
        let suite = json["suite"].stringValue
        
        let address = try self.fetchOrCreateEntity(with: latitude, and: longitude, and: suite)
        
        address.street = json["street"].stringValue
        address.city = json["city"].stringValue
        address.zipcode = json["zipcode"].stringValue
        
        return address
    }
    
    // MARK: - Private API
    
    private func fetchOrCreateEntity(with latitude: String, and longitude: String, and suite: String) throws -> Address {
        let address: Address
        if let fetchedAddress = try self.fetchedEntity(with: latitude, and: longitude, and: suite) {
            address = fetchedAddress
        } else {
            guard let createdAddress = NSManagedObject(entity: self.entity, insertInto: self.managedContext) as? Address else {
                throw ContentsTransformerManager.TransformerError.invalidEntity(identifier: Constants.entityName)
            }
            
            address = createdAddress
            address.latitude = latitude
            address.longitude = longitude
            address.suite = suite
        }
        
        return address
    }
    
    private func fetchedEntity(with latitude: String, and longitude: String, and suite: String) throws -> Address? {
        self.fetchedResultsController.fetchRequest.predicate = NSPredicate(format: "self.latitude == %@ AND self.longitude == %@ AND self.suite == %@", latitude, longitude, suite)
        try self.fetchedResultsController.performFetch()
        return self.fetchedResultsController.fetchedObjects?.first
    }
}
