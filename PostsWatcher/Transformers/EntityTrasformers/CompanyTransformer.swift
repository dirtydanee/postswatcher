//
//  CompanyTransformer.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import SwiftyJSON

/*
 "name": "Romaguera-Crona",
 "catchPhrase": "Multi-layered client-server neural-net",
 "bs": "harness real-time e-markets" 
 */

final class CompanyTransformer {
    
    private struct Constants {
        static let entityName = "Company"
        static let defaultSortDescriptorKey = "name"
    }
    
    private let entity: NSEntityDescription
    private let fetchedResultsController: NSFetchedResultsController<Company>
    
    let managedContext: NSManagedObjectContext
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        
        let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: Constants.defaultSortDescriptorKey, ascending: true)]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
            
        guard let entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: self.managedContext) else {
            throw CoreDataStack.CoreDataError.entitiyCreationFailed(identifier: Constants.entityName)
        }
        self.entity = entity
    }
    
    // MARK: - Public API
    
    func transformSubEntity(json: JSON) throws -> Company {
        let name = json["name"].stringValue
        let company = try self.fetchOrCreateEntity(with: name)
        
        company.catchPhrase = json["catchPhrase"].stringValue
        company.bs = json["bs"].stringValue
        
        return company
    }
    
    // MARK: - Private API
    
    private func fetchOrCreateEntity(with name: String) throws -> Company {
        let company: Company
        if let fetchedCompany = try self.fetchedEntity(with: name) {
            company = fetchedCompany
        } else {
            guard let createdCompany = NSManagedObject(entity: self.entity, insertInto: self.managedContext) as? Company else {
                throw ContentsTransformerManager.TransformerError.invalidEntity(identifier: Constants.entityName)
            }
            company = createdCompany
            company.name = name
        }
        
        return company
    }
    
    private func fetchedEntity(with name: String) throws -> Company? {
        self.fetchedResultsController.fetchRequest.predicate = NSPredicate(format: "self.name == %@", name)
        try self.fetchedResultsController.performFetch()
        return self.fetchedResultsController.fetchedObjects?.first
    }
}
