//
//  PostsStore.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import CoreData

final class PostsStore: NSObject, ContentStore {
    
    private struct Constants {
        static let entityName = "Post"
        static let defaultSortDescriptorKey = "user.email"
    }
    
    var contentIdentifier: ContentIdentifier {
        return .posts
    }
   
    let entity: NSEntityDescription
    let managedContext: NSManagedObjectContext
    let fetchedResultsController: NSFetchedResultsController<Post>
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        
        let fetchRequest: NSFetchRequest<Post> = Post.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: Constants.defaultSortDescriptorKey, ascending: true)]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        
        guard let entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: self.managedContext) else {
            throw CoreDataStack.CoreDataError.entitiyCreationFailed(identifier: Constants.entityName)
        }
        self.entity = entity

        super.init()
        self.fetchedResultsController.delegate = self
    }
    
    // MARK: Public API
    
    func post(with identifier: Int64) throws -> Post? {
        let predicate = NSPredicate(format: "id == \(identifier)")
        return try self.fetchEntity(with: predicate)
    }
    
    func posts(forUser identifier: Int64) throws -> [Post] {
        let predicate = NSPredicate(format: "userId == \(identifier)")
        return try self.fetchEntities(with: predicate)
    }
    
    func posts(containing text: String) throws -> [Post] {
        let predicate = NSPredicate(format: "title CONTAINS[cd] %@ OR user.email CONTAINS[cd] %@", text, text)
        return try self.fetchEntities(with: predicate) 
    }
    
    func allPosts() throws -> [Post] {
        return try self.fetchEntities(with: nil) 
    }
    
    func delete(_ post: Post) throws {
        self.managedContext.delete(post)
        try self.managedContext.save()
    }
}

extension PostsStore: NSFetchedResultsControllerDelegate {}
