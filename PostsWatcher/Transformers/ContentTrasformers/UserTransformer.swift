//
//  UserTransformer.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import SwiftyJSON
import CoreData

final class UserTransformer: ContentTransformer {
    
    var identifier: ContentIdentifier {
        return .users
    }
    
    let store: UsersStore
    let managedContext: NSManagedObjectContext
    
    private let addressTransformer: AddressTransformer
    private let companyTransformer: CompanyTransformer
    private let postsStore: PostsStore
    private let albumsStore: AlbumsStore
    
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        self.store = try UsersStore(managedContext: managedContext)
        self.albumsStore = try AlbumsStore(managedContext: managedContext)
        self.postsStore = try PostsStore(managedContext: managedContext)
        self.addressTransformer = try AddressTransformer(managedContext: managedContext)
        self.companyTransformer = try CompanyTransformer(managedContext: managedContext)
    }
    
    // MARK: - Public API
    
    func transform(json: JSON) throws {
        
        let userID = json["id"].int64Value
        let user: User = try self.store.fetchOrCreateEntity(with: userID)
        user.id = userID
        user.name = json["name"].stringValue
        user.username = json["username"].stringValue
        user.email = json["email"].stringValue
        user.phone = json["phone"].stringValue
        user.website = json["website"].stringValue
        user.address = try self.addressTransformer.transformSubEntity(json: json["address"])
        user.company = try self.companyTransformer.transformSubEntity(json: json["company"])
        user.albums = try self.albumsStore.albums(forUser: userID)
        let posts = try self.postsStore.posts(forUser: userID)
        user.posts = Set(posts)
    }
}
