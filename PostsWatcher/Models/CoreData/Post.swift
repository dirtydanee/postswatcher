//
//  File.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 07.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import Foundation

public class Post: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Post> {
        return NSFetchRequest<Post>(entityName: "Post")
    }
    
    @NSManaged public var id: Int64
    @NSManaged public var userId: Int64
    @NSManaged public var title: String
    @NSManaged public var body: String
    @NSManaged public var user: User
    
}
