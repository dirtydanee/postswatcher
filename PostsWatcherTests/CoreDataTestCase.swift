//
//  CoreDataTestCase.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 06.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import CoreData
@testable import PostsWatcher

class CoreDataTestCase: XCTestCase {
    
    enum CoreDataTestError: Error {
        case unableToCreateEntity(identifier: String)
    }
    
    var managedContext: NSManagedObjectContext!
    
    override func setUp() {
        super.setUp()
        self.managedContext = setUpInMemoryManagedObjectContext()
    }
    
    override func tearDown() {
        self.managedContext = nil
        super.tearDown()
    }
    
    func setUpInMemoryManagedObjectContext() -> NSManagedObjectContext {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle.main])!
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
        } catch {
            print("Adding in-memory persistent store failed")
        }
        
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        return managedObjectContext
    }
}
