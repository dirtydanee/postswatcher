//
//  PostsWatcherPresenter.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 04.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class PostsWatcherPresenter: Presenter {
    
    /// State of the presention for PostsWatcherPresenter viewController based on the application's content availability
    ///
    /// - initial: viewController just been loaded
    /// - loading: presentable content is being loaded
    /// - ready: presentable data is ready
    enum PresentationState {
        case initial
        case loading
        case ready
    }
    
    var state: PresentationState = .initial {
        didSet {
            switch state {
            case .loading:
                self.postsWatcherSplitViewController.presentLoadingLayer()
            case .ready:
                self.postsWatcherSplitViewController.removeLoadingLayer()
            case .initial:
                break
            }
        }
    }
    
    private struct Constants {
        static let preferedMasterWidth: CGFloat = 320
    }
    
    var viewController: UIViewController {
        return self.postsWatcherSplitViewController
    }
    
    var parent: Presenter?
    
    private let postsDetailPresenter: PostDetailPresenter
    private let postsPresenter: PostsPresenter
    private let postsWatcherSplitViewController: PostsWatcherSplitViewController
    
    init() {
        self.postsDetailPresenter = PostDetailPresenter()
        self.postsPresenter = PostsPresenter()
        self.postsWatcherSplitViewController = PostsWatcherSplitViewController()
        self.postsWatcherSplitViewController.viewControllers = [postsPresenter.viewController, postsDetailPresenter.viewController]
        self.postsWatcherSplitViewController.preferredPrimaryColumnWidthFraction = Constants.preferedMasterWidth / UIScreen.main.bounds.width 
        self.postsWatcherSplitViewController.preferredDisplayMode = .allVisible
        self.postsDetailPresenter.viewController.view.isHidden = true
        self.postsDetailPresenter.parent = self
        self.postsPresenter.parent = self
    }
    
    func notify(event: Event) {
        switch event {
        case .postDataSourceBecameAvailable(dataSource: _):
            self.postsPresenter.notify(event: event)
        case .didSelect(postViewModel: _):
            self.postsDetailPresenter.notify(event: event)
        case .didDelete(postViewModel: _):
            self.postsDetailPresenter.viewController.view.isHidden = true
        }
    }
}
