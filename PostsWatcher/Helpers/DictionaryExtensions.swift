//
//  DictionaryExtensions.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation

extension Dictionary {
    
    enum DictionaryError: Error {
        case missingValue(forKey: Key)
    }
    
    var valuesArray: [Value] {
        return Array(self.values)
    }
    
    func throwingValue(for key: Key) throws -> Value {
        guard let value = self[key] else {
            throw DictionaryError.missingValue(forKey: key)
        }
        
        return value
    }
}
