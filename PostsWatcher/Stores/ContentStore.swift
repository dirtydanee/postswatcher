//
//  ContentStore.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 08.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData

enum ContentStoreError: Error {
    case invalidEntity(identifier: String)
}

/// Unified interface for core data entity stores to generalize fetching and creation
protocol ContentStore {
    
    /// The identifier of the Content the store is responsible to handle
    var contentIdentifier: ContentIdentifier { get }
    
    /// The managedObjectContext the store is using for fetches
    var managedContext: NSManagedObjectContext { get }
    
    /// The description of the entity the store is handling
    var entity: NSEntityDescription { get }
    
    /// The associated type of `NSFetchRequestResult`
    associatedtype T: NSFetchRequestResult
    
    /// FetchedResultsController of the ContentStore
    var fetchedResultsController: NSFetchedResultsController<T> { get }
}

extension ContentStore {

    /// Check if an entity is persisted already, otherwise create a new one 
    ///
    /// - Parameter identifier: The identifier of the entity to be checked
    /// - Returns: A new core data entity
    /// - Throws: Errors associated to `ContentStore` or `CoreData`
    func fetchOrCreateEntity<T>(with identifier: Int64) throws -> T {
        let entity: T
        let predicate = NSPredicate(format: "id == \(identifier)")
        if let fetchedEntity: T = try self.fetchEntity(with: predicate) {
            entity = fetchedEntity
        } else {
            entity = try self.createNewEntity()
        }
        
        return entity
    }
    
    /// Fetching entity with predicate
    ///
    /// - Parameter predicate: The predicate to be executed upon fetch
    /// - Returns: The entity, or `nil` if not found
    /// - Throws: Errors associated to `CoreData`
    func fetchEntity<T>(with predicate: NSPredicate?) throws -> T? {
       return try self.fetchEntities(with: predicate).first
    }
    
    /// Fetching mulitple entities with predicate
    ///
    /// - Parameter predicate: The predicate to be executed upon fetch
    /// - Returns: The entity, or empty array if not found
    /// - Throws: Errors associated to `CoreData`
    func fetchEntities<T>(with predicate: NSPredicate?) throws -> [T] {
        self.fetchedResultsController.fetchRequest.predicate = predicate
        try self.fetchedResultsController.performFetch()
        return self.fetchedResultsController.fetchedObjects as? [T] ?? []
    }
    
    /// Creating new entity
    /// NOTE: Context is NOT being saved upon calling this function 
    ///
    /// - Returns: The freshly created entity
    /// - Throws: Errors associated to `ContentStore`
    func createNewEntity<T>() throws -> T {
        guard let entity = NSManagedObject(entity: self.entity, insertInto: self.managedContext) as? T else {
            throw ContentStoreError.invalidEntity(identifier: self.contentIdentifier.rawValue)
        }
        return entity
    }
}
