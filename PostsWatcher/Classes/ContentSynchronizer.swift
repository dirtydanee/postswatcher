//
//  ContentSynchronizer.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation

final class ContentSynchronizer {
    
    private let contentsFetcher: ContentsFetcher
    private let contentsTransformerManager: ContentsTransformerManager
    typealias ContentSynchronizerCompletionHandler = (_ success: Bool) -> Void
    
    init(coreDataStack: CoreDataStack) throws {
        self.contentsFetcher = ContentsFetcher()
        let backgroundObjectContext = coreDataStack.persistentContainer.newBackgroundContext()
        self.contentsTransformerManager = try ContentsTransformerManager(managedContext: backgroundObjectContext)
    }
    
    func startSyncing(completion: @escaping ContentSynchronizerCompletionHandler) {
        
        do {
            let postsRequest = try ContentRequestFactory.makeContentDataRequest(forIdentifier: .posts)
            let usersRequest = try ContentRequestFactory.makeContentDataRequest(forIdentifier: .users)
            let albumRequest = try ContentRequestFactory.makeContentDataRequest(forIdentifier: .albums)
            let photosRequest = try ContentRequestFactory.makeContentDataRequest(forIdentifier: .photos)
            
            self.contentsFetcher.fetchContent(from: [postsRequest, usersRequest, albumRequest, photosRequest], completion: { [weak self] 
                (success, error, contentData) in
                guard let `self` = self else {
                    clog("ContentsFetcher has been deallocated while dowloading content", priority: .error)
                    completion(false)
                    return
                }
                
                guard error == nil else {
                    clog("An error has occoured while ApplicationContentPresenter was downloading content data. ERROR: \(error!.localizedDescription)", priority: .error)
                    completion(false)
                    return
                }
                
                guard success, let contentData = contentData else {
                    clog("An unknown error has occoured while ApplicationContentPresenter was downloading content data", priority: .error)
                    completion(false)
                    return
                }
                self.transform(contentData, completion: completion)
            })
            
        } catch let e {
            clog("Error occurred while fetching content. ERROR: \"\(e.localizedDescription)\"", priority: .error)
        }
    }
    
    private func transform(_ contentData: [ContentData], completion: @escaping ContentSynchronizerCompletionHandler) {
        do {
            try contentsTransformerManager.transform(contentDatas: contentData, completion: { (success) in
                completion(success)
            })
        } catch let e {
            clog("Error occurred while transforming content. ERROR: \"\(e.localizedDescription)\"", priority: .error)
            completion(false)
        }
    }
}
