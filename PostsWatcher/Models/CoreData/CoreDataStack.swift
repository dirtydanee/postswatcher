//
//  CoreDataStack.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataStack {
    
    enum CoreDataError: Error {
        case entitiyCreationFailed(identifier: String)
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "PostsWatcher")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let nserror = error as NSError? {
                clog("Unresolved error \(nserror), \(nserror.userInfo)", priority: .error)
                fatalError()
            }
        })
        return container
    }()
    
    // MARK: - Saving 
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                clog("Unresolved error \(nserror), \(nserror.userInfo)", priority: .error)
                fatalError()
            }
        }
    }

}
