//
//  ContentFetcher.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import Alamofire

final class ContentsFetcher {

    private typealias DataRequestCompletion = (_ success: Bool, _ error: Error?, _ data: Data?) -> Void
    private var contentDatas: [ContentData] = [] 
    private var requests: [ContentDataRequest] = []
    
    typealias ContentDatasFetchingCompletion = (_ success: Bool, _ error: Error?, _ datas: [ContentData]?) -> Void

    /// Fetch content from multiple content data requests
    ///
    /// - Parameters:
    ///   - requests: The reqquests where we are expected to fetch the data from
    ///   - completion: data if successful, otherwise error
    func fetchContent(from requests: [ContentDataRequest], completion: @escaping ContentDatasFetchingCompletion) {
        self.requests = requests
        self.startFetching(request: self.requests.first, completion: completion)
    }
    
    // MARK: Private API
    
    private func startFetching(request: ContentDataRequest?, completion: @escaping ContentDatasFetchingCompletion) {
        if let request = request {
            clog("ContentsFetcher has started fetching request with identifier: \(request.identifier)", priority: .debug)
            self.requests.removeFirst()
            self.fetchData(request: request) { [weak self] (success, error, data) in
                
                guard let `self` = self else {
                    clog("ContentsFetcher has been deallocated while dowloading content", priority: .error)
                    completion(false, nil, nil)
                    return 
                }
                
                if let error = error {
                    clog("Downloading content has been failed with for the request with the following identifier: \(request.identifier). ERROR: \(error)", priority: .error)
                    completion(false, error, nil)
                    return
                }
                
                guard success, let data = data else {
                    clog("Downloading content has been failed for the request with the following identifier: \(request.identifier)", priority: .debug)
                    completion(false, nil, nil)
                    return
                }
                
                let contentData = ContentData(identifier: request.identifier, data: data)
                self.contentDatas.append(contentData)
                clog("ContentsFetcher finished fetching successfully request with identifier: \(request.identifier)", priority: .debug)
                self.startFetching(request: self.requests.first, completion: completion)
            }     
        } 
        else {
            completion(true, nil, self.contentDatas)
        }
    }
    
    private func fetchData(request: ContentDataRequest,  completion: @escaping DataRequestCompletion) {
        request.dataRequest.response { (response: DefaultDataResponse) in
            
            guard response.error == nil else {
                clog("An error has occoured while dowloading data with identifier: \(request.identifier.rawValue). ERROR: \(response.error!.localizedDescription)", priority: .error)
                completion(false, response.error, nil)
                return
            }
            
            guard let data = response.data else {
                clog("An unknown error has occoured while dowloading data with identifier: \(request.identifier.rawValue)", priority: .error)
                completion(false, nil, nil)
                return
            }
            
            
            completion(true, nil, data)
        }
        request.dataRequest.resume()
    }
} 
