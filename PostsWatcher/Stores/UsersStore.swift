//
//  UsersStore.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import CoreData

final class UsersStore: NSObject, ContentStore {
    
    private struct Constants {
        static let entityName = "User"
        static let defaultSortDescriptorKey = "name"
    }
    
    var contentIdentifier: ContentIdentifier {
        return .users
    }
    
    let entity: NSEntityDescription
    let managedContext: NSManagedObjectContext
    let fetchedResultsController: NSFetchedResultsController<User>
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        
        let fetchRequest: NSFetchRequest<User> = User.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: Constants.defaultSortDescriptorKey, ascending: true)]
        
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        
        guard let entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: self.managedContext) else {
            throw CoreDataStack.CoreDataError.entitiyCreationFailed(identifier: Constants.entityName)
        }
        self.entity = entity
        
        super.init()
        self.fetchedResultsController.delegate = self
    }
    
    func user(with identifier: Int64) throws -> User? {
        let predicate = NSPredicate(format: "id == \(identifier)")
        return try self.fetchEntity(with: predicate)
    }
}

extension UsersStore: NSFetchedResultsControllerDelegate {}
