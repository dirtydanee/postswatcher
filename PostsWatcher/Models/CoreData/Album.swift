//
//  Album.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 07.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import Foundation

public class Album: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Album> {
        return NSFetchRequest<Album>(entityName: "Album")
    }
    
    @NSManaged public var id: Int64
    @NSManaged public var userId: Int64
    @NSManaged public var title: String
    @NSManaged public var photos: Set<Photo>
}
