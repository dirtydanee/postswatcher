//
//  PostDetailHeaderView.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 04.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class PostDetailHeaderView: UITableViewHeaderFooterView {
    
    static let identifier: String = String(describing: PostDetailHeaderView.self)
    
    private struct Layout {
        static let titleLabelToBodyLabelVerticalSpace: CGFloat = 10
        static let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 10, left: 16, bottom: 16, right: 10) 
    }
    
    private var titleLabel: UILabel!
    private var bodyLabel: UILabel!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        let titleLabel = UILabel(frame: .zero)
        let bodyLabel = UILabel(frame: .zero)
        titleLabel.font = UIFont.systemFont(ofSize: 22, weight: UIFontWeightSemibold)
        bodyLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular)
        [titleLabel, bodyLabel].forEach { (label) in
            label.textAlignment = .center
            label.numberOfLines = 0
            self.addSubview(label)
        }
        self.titleLabel = titleLabel
        self.bodyLabel = bodyLabel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Overrides
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let maxWidth = self.width - (Layout.contentInsets.left + Layout.contentInsets.right)
        self.titleLabel.resize(for: maxWidth)
        self.bodyLabel.resize(for: maxWidth)
        
        self.titleLabel.origin.x = floor((maxWidth - self.titleLabel.width) / 2) + Layout.contentInsets.left
        self.titleLabel.origin.y = Layout.contentInsets.top

        let titleLabelHeight = self.titleLabel.sizeThatFits(CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude)).height
        self.bodyLabel.origin.x =  floor((maxWidth - self.bodyLabel.width) / 2) + Layout.contentInsets.left
        self.bodyLabel.origin.y = Layout.contentInsets.top + titleLabelHeight + Layout.titleLabelToBodyLabelVerticalSpace 
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let maxWidth = size.width - (Layout.contentInsets.left + Layout.contentInsets.right) 
        let titleLabelHeight = self.titleLabel.sizeThatFits(CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude)).height
        let bodyLabelHeight = self.bodyLabel.sizeThatFits(CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude)).height
        let height = (Layout.contentInsets.top + Layout.contentInsets.bottom) + Layout.titleLabelToBodyLabelVerticalSpace + titleLabelHeight + bodyLabelHeight
        return CGSize(width: size.width, height: height)
    }
    
    // MARK: - Public API
    
    func configure(with viewModel: PostViewModel) {
        self.titleLabel.text = viewModel.title
        self.bodyLabel.text = viewModel.body
    }
}
