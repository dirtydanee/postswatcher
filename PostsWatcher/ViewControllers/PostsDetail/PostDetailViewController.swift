//
//  PostDetailViewController.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 04.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class PostDetailViewController: UIViewController {
    
    var tableView: UITableView!
    var dataSource: PostDetailDataSource? {
        didSet {
            guard let dataSource = dataSource else { return }
            self.setupHeaderView(with: dataSource.viewModel)
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.frame.size = self.view.frame.size
        self.sizeHeaderToFit()
    }
    
    // MARK: Private API
    
    private func sizeHeaderToFit() {
        guard let headerView = tableView.tableHeaderView else { return }
        headerView.frame.size = headerView.sizeThatFits(CGSize(width: self.view.frame.width, height: CGFloat.greatestFiniteMagnitude))
    }
    
    private func setupTableView() {
        let tableView = UITableView(frame: CGRect(origin: .zero, size: .zero), style: .plain)
        tableView.tableFooterView = UIView()
        
        tableView.register(PostDetailHeaderView.self, forHeaderFooterViewReuseIdentifier: PostDetailHeaderView.identifier)
        tableView.register(CollapsableSectionHeaderView.self, forHeaderFooterViewReuseIdentifier: CollapsableSectionHeaderView.identifier)
        tableView.register(UINib(nibName: "PhotoTableViewCell", bundle: nil), forCellReuseIdentifier: PhotoTableViewCell.identifier)
        
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 44
            
        self.view.addSubview(tableView)
        self.tableView = tableView
    }
    
    private func setupHeaderView(with viewModel: PostViewModel) {
        guard let postDetailHeaderView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: PostDetailHeaderView.identifier) as? PostDetailHeaderView else {
            clog("Unable to typecast reusable PostDetailHeaderView in PostDetailViewController", priority: .error)
            return
        }
        postDetailHeaderView.configure(with: viewModel)
        self.tableView.tableHeaderView = postDetailHeaderView
        self.sizeHeaderToFit()
    }
}
