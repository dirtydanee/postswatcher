//
//  UserStoreTests.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 08.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import CoreData
@testable import PostsWatcher

class UserStoreTests: CoreDataTestCase {
    
    var store: UsersStore!
    
    override func setUp() {
        super.setUp()
        self.store = try? UsersStore(managedContext: self.managedContext)
    }
    
    override func tearDown() {
        self.store = nil
        super.tearDown()
    }

    
    func testfetchUserWithExistingIdentifier() throws {
        let identifier = Int64(1000)
        try createEntity(with: identifier)
        XCTAssertNotNil(try self.store.user(with: identifier))
    }
    
    func testfetchUserWithNonExistingIdentifier() throws {
        XCTAssertNil(try self.store.user(with: Int64(-1)))
    }
    
    @discardableResult
    private func createEntity(with identifier: Int64) throws -> User {
        guard let user = NSManagedObject(entity: self.store.entity, insertInto: self.managedContext) as? User else {
            throw CoreDataTestError.unableToCreateEntity(identifier: self.store.contentIdentifier.rawValue)
        }
        user.id = identifier
        return user
    }  
}
