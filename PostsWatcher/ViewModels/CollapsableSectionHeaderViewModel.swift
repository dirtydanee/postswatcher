//
//  CollapsableSectionHeaderViewModel.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 06.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation

struct CollapsableSectionHeaderViewModel {
    let sectionIdentifier: Int
    let albumViewModel: AlbumViewModel
    var collapsedState: CollapsableSectionHeaderView.CollapsedState
    
    mutating func update(to state: CollapsableSectionHeaderView.CollapsedState) {
        self.collapsedState = state
    }
}
