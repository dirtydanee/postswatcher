//
//  PhotoTransformerTests.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 06.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import PostsWatcher

class PhotoTransformerTests: CoreDataTestCase {
    
    func testTransformation() throws {
        
        let transformer = try PhotoTransformer(managedContext: self.managedContext)
        let userData = try Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "Photo", ofType:"json")!))
        let json = JSON(data: userData, options: .allowFragments, error: nil)
        try transformer.transform(json: json)
        
        let photo: Photo = try transformer.store.fetchOrCreateEntity(with: json["id"].int64Value)
        XCTAssertEqual(photo.id, json["id"].int64!)
        XCTAssertEqual(photo.albumId, json["albumId"].int64!)
        XCTAssertEqual(photo.title, json["title"].string!)
        XCTAssertEqual(photo.url, json["url"].string!)
        XCTAssertEqual(photo.thumbnailUrl, json["thumbnailUrl"].string!)
    }
}
