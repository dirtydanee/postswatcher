//
//  ContentIndentifier.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation

/// Main type of contents presented by the application
enum ContentIdentifier: String {
    case posts
    case users
    case albums
    case photos
}
