//
//  PostTableViewCell.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 04.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class PostTableViewCell: UITableViewCell {
    
    static let identifier: String = String(describing: PostTableViewCell.self)

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    
    func configure(with viewModel: PostViewModel) {
        self.titleLabel.text = viewModel.title
        self.emailLabel.text = viewModel.userEmailAddress
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = nil
        self.emailLabel.text = nil
    }
}
