//
//  UserTransformerTests.swift
//  PostsWatcherTests
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import PostsWatcher

class UserTransformerTests: CoreDataTestCase {
    
    func testTransformation() throws {
        
        let transformer = try UserTransformer(managedContext: self.managedContext)
        let userData = try Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "User", ofType:"json")!))
        let json = JSON(data: userData, options: .allowFragments, error: nil)
        try transformer.transform(json: json)
        
        let user: User = try transformer.store.fetchOrCreateEntity(with: json["id"].int64Value)
        XCTAssertEqual(user.id, json["id"].int64!)
        XCTAssertEqual(user.name, json["name"].string!)
        XCTAssertEqual(user.username, json["username"].string!)
        XCTAssertEqual(user.email, json["email"].string!)
        XCTAssertEqual(user.phone, json["phone"].string!)
        XCTAssertEqual(user.website, json["website"].string!)
        XCTAssertEqual(user.address.street, json["address"]["street"].string!)
        XCTAssertEqual(user.address.city, json["address"]["city"].string!)
        XCTAssertEqual(user.address.suite, json["address"]["suite"].string!)
        XCTAssertEqual(user.address.zipcode, json["address"]["zipcode"].string!)
        XCTAssertEqual(user.address.latitude, json["address"]["geo"]["lat"].string!)
        XCTAssertEqual(user.address.longitude, json["address"]["geo"]["lng"].string!)
        XCTAssertEqual(user.company.bs, json["company"]["bs"].string!)
        XCTAssertEqual(user.company.name, json["company"]["name"].string!)
        XCTAssertEqual(user.company.catchPhrase, json["company"]["catchPhrase"].string!)
    }
}
