//
//  AlbumsStore.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import CoreData

final class AlbumsStore: NSObject, ContentStore {
    
    private struct Constants {
        static let entityName = "Album"
        static let defaultSortDescriptorKey = "title"
    }
    
    var contentIdentifier: ContentIdentifier {
        return .albums
    }
    
    let entity: NSEntityDescription
    let managedContext: NSManagedObjectContext
    let fetchedResultsController: NSFetchedResultsController<Album>
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        
        let fetchRequest: NSFetchRequest<Album> = Album.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: Constants.defaultSortDescriptorKey, ascending: true)]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        
        guard let entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: self.managedContext) else {
            throw CoreDataStack.CoreDataError.entitiyCreationFailed(identifier: Constants.entityName)
        }
        self.entity = entity

        super.init()
        self.fetchedResultsController.delegate = self
    }
    
    func album(with identifier: Int64) throws -> Album? {
        let predicate = NSPredicate(format: "id == \(identifier)")
        return try self.fetchEntity(with: predicate)
    }
    
    func albums(forUser identifier: Int64) throws -> Set<Album> {
        let predicate = NSPredicate(format: "userId == \(identifier)")
        let albums: [Album] = try self.fetchEntities(with: predicate)
        return Set(albums)
    }
}

extension AlbumsStore: NSFetchedResultsControllerDelegate {}
