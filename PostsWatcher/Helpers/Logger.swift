//
//  Logger.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation

enum LoggingPriority: String, CustomStringConvertible {
    case general
    case debug
    case error
    
    var description: String {
        return self.rawValue.uppercased()
    }
    
    static let `default`: LoggingPriority = .debug
}

func clog(_ message: String, priority: LoggingPriority, file: StaticString = #file, line: Int = #line) {
    if priority == .general || priority == .error {
        print("\(Date()) \(applicationName)[\(priority.description)][\(stripFilePath(from: file.description)):\(line)]: \(message)")
    } else if LoggingPriority.default == priority {
         print("\(Date()) \(applicationName)[\(priority.description)][\(stripFilePath(from: file.description)):\(line)]: \(message)")
    }
}

private func stripFilePath(from file: String) -> String {
    let firstSlashIndex = file.range(of: "/", options: .backwards)!.lowerBound
    let rightPosition = file.index(firstSlashIndex, offsetBy: 1)
    return file.substring(from: rightPosition)
}

private var applicationName: String {
    return Bundle.main.object(forInfoDictionaryKey: "CFBundleName")! as! String
}

