//
//  PostTransformerTests.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 06.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import PostsWatcher

class PostTransformerTests: CoreDataTestCase {
    
    func testTransformation() throws {
        
        let transformer = try PostTransformer(managedContext: self.managedContext)
        let userData = try Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "Post", ofType:"json")!))
        let json = JSON(data: userData, options: .allowFragments, error: nil)
        try transformer.transform(json: json)
        
        let post: Post = try transformer.store.fetchOrCreateEntity(with: json["id"].int64Value)
        XCTAssertEqual(post.id, json["id"].int64!)
        XCTAssertEqual(post.userId, json["userId"].int64!)
        XCTAssertEqual(post.body, json["body"].string!)
        XCTAssertEqual(post.title, json["title"].string!)
    }
}
