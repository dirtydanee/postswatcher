# UI Feedback 
The application at the moment is using native iOS components following Apple's [Human Interface Guideline][1].

## Possible improvements
### Splash screen
* Introduce a splash screen containing artwork
* Before presenting the applications user interface, at the minute the loading indicator with a grey overlay on top of the `UISplitViewController`, run a simple, fast but entertaining animation.
 
### Enable MasterView to be hidden / shown
* Currently the MasterView is always visible, however looking at a post could be done on full screen.

### Introduce color schema 
* Currently the application is only using black and white colours, but a smart choice of a colour palette is a must.

### Animations
* Introduce fast and eye catching animations, when the user interacts with the application. 
* Possible places:  
    * Reloading the list of posts upon delete
    * collapsing / uncollapsing album titles
    * Presenting a new post

### Add placeholder when no post is selected
* When there is no post selected currently a black screen is shown in the detail view. Is this not the perfect time to advertise our brand to the user?

### Enable rotation to all orientations
* Users tend to rotate their iPad devices, and they expect to utilise the benefits of a wider screen when using the device in landscape.

### Add extra information for detail view
* When all the sections are collapsed, the bottom of the detail view looks empty. We could add more information about the user as a footer view.

[1]: https://developer.apple.com/ios/human-interface-guidelines/overview/design-principles/
