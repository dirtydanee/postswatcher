//
//  PhotosStore.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import CoreData

final class PhotosStore: NSObject, ContentStore {
    
    private struct Constants {
        static let entityName = "Photo"
        static let defaultSortDescriptorKey = "title"
    }
    
    var contentIdentifier: ContentIdentifier {
        return .photos
    }
    
    let entity: NSEntityDescription
    let managedContext: NSManagedObjectContext
    let fetchedResultsController: NSFetchedResultsController<Photo>
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        
        let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: Constants.defaultSortDescriptorKey, ascending: true)]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        
        guard let entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: self.managedContext) else {
            throw CoreDataStack.CoreDataError.entitiyCreationFailed(identifier: Constants.entityName)
        }
        self.entity = entity
    
        super.init()
        self.fetchedResultsController.delegate = self
    }
    
    func photo(with identifier: Int64) throws -> Photo? {
        let predicate = NSPredicate(format: "id == \(identifier)")
        return try fetchEntity(with: predicate)
    }
    
    func photos(forAlbum albumIdentifier: Int64) throws -> Set<Photo> { 
        let predicate = NSPredicate(format: "albumId == \(albumIdentifier)")
        let photos: [Photo] = try fetchEntities(with: predicate) 
        return Set(photos)
    }
}

extension PhotosStore: NSFetchedResultsControllerDelegate {}
