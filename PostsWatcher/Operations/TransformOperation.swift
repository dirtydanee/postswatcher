//
//  TransformOperation.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import SwiftyJSON

final class TransformOperation: Operation {
    
    let contentTransformer: ContentTransformer
    let json: JSON
    
    init(contentTransformer: ContentTransformer, json: JSON) {
        self.contentTransformer = contentTransformer
        self.json = json
    }
    
    override func main() {
        
        guard !isCancelled else { return }
        clog("operation started with identifier: \(contentTransformer.identifier)", priority: .debug)
        
        do {
            
            guard let rawContents = json.array else {
                throw ContentsTransformerManager.TransformerError.invalidStructure(identifier: contentTransformer.identifier.rawValue)
            }
            
            for rawContent in rawContents {
                try self.contentTransformer.transform(json: rawContent)
            }
            
            clog("operation ended with identifier: \(contentTransformer.identifier)", priority: .debug)
            
        } catch let e {
            clog("Could not transform content with identifier: \(contentTransformer.identifier). Error description: \(e)", priority: .error)
            fatalError()
        }
    }
}
