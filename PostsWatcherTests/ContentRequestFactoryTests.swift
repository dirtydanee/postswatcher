//
//  ContentRequestFactoryTests.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 07.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import Alamofire
@testable import PostsWatcher

class ContentRequestFactoryTests: XCTestCase {
    
    func testContentURLs() throws {
        let postsURL = try ContentRequestFactory.makeContentDataRequest(forIdentifier: .posts)
        XCTAssertEqual("http://jsonplaceholder.typicode.com/posts", postsURL.dataRequest.request!.url!.absoluteString)
        
        let usersURL = try ContentRequestFactory.makeContentDataRequest(forIdentifier: .users)
        XCTAssertEqual("http://jsonplaceholder.typicode.com/users", usersURL.dataRequest.request!.url!.absoluteString)
        
        let albumsURL = try ContentRequestFactory.makeContentDataRequest(forIdentifier: .albums)
        XCTAssertEqual("http://jsonplaceholder.typicode.com/albums", albumsURL.dataRequest.request!.url!.absoluteString)
        
        let photosURL = try ContentRequestFactory.makeContentDataRequest(forIdentifier: .photos)
        XCTAssertEqual("http://jsonplaceholder.typicode.com/photos", photosURL.dataRequest.request!.url!.absoluteString)
    }
}
