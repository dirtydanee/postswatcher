//
//  AlbumViewModel.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 05.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

struct AlbumViewModel {
    
    var title: String {
        return self.album.title
    }
    
    var photoViewModels: [PhotoViewModel] {
        return self.createPhotoViewModels()
    }
    
    let album: Album 
    
    init(album: Album) {
        self.album = album
    }
    
    private func createPhotoViewModels() -> [PhotoViewModel] {
        var photos: [PhotoViewModel] = []
        for photo in self.album.photos {
            let viewModel = PhotoViewModel(photo: photo)
            photos.append(viewModel)
        }
        
        return photos
    }
}
