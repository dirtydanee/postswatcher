//
//  Photo.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 07.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import Foundation

public class Photo: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo")
    }
    
    @NSManaged public var id: Int64
    @NSManaged public var albumId: Int64
    @NSManaged public var thumbnailUrl: String
    @NSManaged public var url: String
    @NSManaged public var title: String
}
