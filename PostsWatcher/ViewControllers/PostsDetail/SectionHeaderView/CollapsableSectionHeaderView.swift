//
//  CollapsableSectionHeaderView.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 05.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

protocol CollapsableSectionHeaderViewDelegate: class {
    
    /// Signaling when headerView instance has update its state
    ///
    /// - Parameters:
    ///   - headerView: The headerView what has been updated its state
    ///   - section: The section of the headerView
    ///   - state: The new state after the update
    func collapsableSectionHeaderView(_ headerView: CollapsableSectionHeaderView, at section: Int, didUpdateTo state: CollapsableSectionHeaderView.CollapsedState)
}

final class CollapsableSectionHeaderView: UITableViewHeaderFooterView {
    
    static let identifier: String = String(describing: CollapsableSectionHeaderView.self) 
    
    enum CollapsedState {
        case open
        case closed
    }
    
    weak var delegate: CollapsableSectionHeaderViewDelegate?
    
    private struct Layout {
        static let imageViewSize = CGSize(width: 15, height: 15)
        static let titleLabelToImageViewHorizontalSpacing: CGFloat = 8
        static let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(10, 16, 10, 16)
    }
    
    private var titleLabel: UILabel!
    private var imageView: UIImageView!
    private var button: UIButton!
    private(set) var state: CollapsedState = .closed      
    private(set) var sectionIdentifier: Int = -1
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    
        let titleLabel = UILabel(frame: .zero)
        titleLabel.textAlignment = .left
        titleLabel.numberOfLines = 0
        self.titleLabel = titleLabel
        self.addSubview(titleLabel)        

        let imageView = UIImageView(frame: CGRect(origin: .zero, size: Layout.imageViewSize))
        imageView.image = UIImage(named: "closed-arrow")
        self.imageView = imageView
        self.addSubview(imageView)
        
        let button = UIButton(frame: .zero)
        button.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
        self.button = button
        self.addSubview(button)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.button.frame = self.bounds
        let maxLabelWidth = self.width - Layout.contentInsets.left - Layout.titleLabelToImageViewHorizontalSpacing - Layout.imageViewSize.width - Layout.contentInsets.right
        self.titleLabel.resize(for: maxLabelWidth)
        self.titleLabel.origin = CGPoint(x: Layout.contentInsets.left, y: Layout.contentInsets.top) 
        self.imageView.origin = CGPoint(x: self.width - Layout.contentInsets.right - Layout.imageViewSize.width, y: 0)
        self.imageView.centerY = self.titleLabel.centerY
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
         let maxLabelWidth = self.width - Layout.contentInsets.left - Layout.titleLabelToImageViewHorizontalSpacing - Layout.imageViewSize.width - Layout.contentInsets.right
        let titleLabelHeight = self.titleLabel.sizeThatFits(CGSize(width: maxLabelWidth, height: CGFloat.greatestFiniteMagnitude)).height
        let height = Layout.contentInsets.top + titleLabelHeight + Layout.contentInsets.bottom
        return CGSize(width: size.width, height: height)
    }
    
    // MARK: - Public API
    
    func configure(viewModel: CollapsableSectionHeaderViewModel) {
        self.titleLabel.text = viewModel.albumViewModel.title
        self.sectionIdentifier = viewModel.sectionIdentifier
        self.state = viewModel.collapsedState
        self.rotateIcon(animated: false, completion: nil)
    }
    
    // MARK: - Private API
    @objc
    private func didTapButton(_ sender: UIButton) {
        self.toogleState()
        self.rotateIcon(animated: true, completion: { [weak self] in 
            guard let `self` = self else {
                clog("CollapsableSectionHeaderView has been deallocated while running animation", priority: .debug)
                return 
            }
            self.delegate?.collapsableSectionHeaderView(self, at: self.sectionIdentifier, didUpdateTo: self.state)
        })
    }
    
    private func toogleState() {
        self.state = self.state == .open ? .closed : .open
    }
    
    private func rotateIcon(animated: Bool, completion: (() -> Void)? = nil) {
        let transformation = self.state == .open ? CGAffineTransform(rotationAngle: .pi) : .identity
        let duration: TimeInterval = animated ? 0.33 : 0
        UIView.animate(withDuration: duration, animations: { 
            self.imageView.transform = transformation    
        }) { (finished) in
            completion?()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.state = .closed
        self.titleLabel.text = nil
    }
}
