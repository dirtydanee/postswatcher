//
//  PostViewModel.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

struct PostViewModel {
    
    var title: String {
        return post.title
    }
    
    var body: String {
        return post.body
    }
    
    var userEmailAddress: String {
        return post.user.email
    }
    
    var albums: [AlbumViewModel] {
        return self.createAlbumViewModels()
    }
    
    let post: Post
    
    init(post: Post) {
        self.post = post
    }
    
    private func createAlbumViewModels() -> [AlbumViewModel] {
        var albums: [AlbumViewModel] = []
        for album in self.post.user.albums {
            let viewModel = AlbumViewModel(album: album)
            albums.append(viewModel)
        }
        
        return albums
    }
}
