//
//  SaveOperation.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 03.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Foundation
import CoreData

final class SaveOperation: Operation {
    
    let managedContext: NSManagedObjectContext
    
    init(managedContext: NSManagedObjectContext) {
        self.managedContext = managedContext
    }

    override func main() {
        guard !isCancelled else { return }
        clog("Save operation has been started", priority: .debug)
        do {
            try self.managedContext.save()
            clog("Save operation has finished", priority: .debug)
        } catch let e {
            clog("Save operation unable to executed. ERROR: \(e)", priority: .error)
            fatalError()
        }
    }
}
