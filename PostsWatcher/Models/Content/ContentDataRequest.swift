//
//  ContentDataRequest.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import Alamofire

// Wrapper on DataRequest for convinient identification 
struct ContentDataRequest {
    
    /// The identifier of the data
    let identifier: ContentIdentifier
    
    /// The request for downloading the content under the identifier
    let dataRequest: DataRequest
}
