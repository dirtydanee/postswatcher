//
//  AlbumTransformer.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import SwiftyJSON

final class AlbumTransformer: ContentTransformer {
    
    var identifier: ContentIdentifier {
        return .albums
    }
    
    let store: AlbumsStore
    let managedContext: NSManagedObjectContext
    private let photosStore: PhotosStore
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        self.store = try AlbumsStore(managedContext: managedContext)
        self.photosStore = try PhotosStore(managedContext: managedContext)
    }
    
    // MARK: - Public API
    
    func transform(json: JSON) throws {
        let albumId = json["id"].int64Value
        let album: Album = try self.store.fetchOrCreateEntity(with: albumId)
        album.id = albumId
        album.userId = json["userId"].int64Value
        album.title = json["title"].stringValue
        album.photos = try self.photosStore.photos(forAlbum: albumId) 
    }
}
