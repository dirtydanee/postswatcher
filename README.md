# PostsWatcher for iOS

## What does it do?
An iPad application allowing users to download and watch posts from different users. A post contains title, description, the email address of the user who posted it and various photo albums.

## Requirements 
* Swift 3.0
* iOS 10+
* Cocoapods version 1.2.1

## How to install?
* Clone this repository to your computer
* Install Cocoapods  
    * run in the terminal `sudo gem install cocoapods`
* run in the terminal `pod install`
* after successfully initialsing the workspace, please start `PostsWatcher.xcworkspace`

## Any questions?
Reach out to me at daniel.metzing@gmail.com


