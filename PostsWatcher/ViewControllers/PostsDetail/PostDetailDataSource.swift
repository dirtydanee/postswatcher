//
//  PostDetailDataSource.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 04.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

protocol PostDetailDataSourceDelegate: class {
    
    /// DataSource asking for an update at a given section
    ///
    /// - Parameters:
    ///   - dataSource: The datasource requesting the update
    ///   - section: The section to be updated
    func postDetailDataSource(_ dataSource: PostDetailDataSource, sectionNeedsUpdatingAt section: Int)
} 

final class PostDetailDataSource: NSObject {
    
    let viewModel: PostViewModel
    weak var delegate: PostDetailDataSourceDelegate?
    fileprivate var headerViewModels: [CollapsableSectionHeaderViewModel] = []

    init(viewModel: PostViewModel) {
        self.viewModel = viewModel
        super.init()
        self.createHeaderViewModels()
    }
    
    // MARK: Private API
    
    private func createHeaderViewModels() {
        for (index, albumViewModel) in self.viewModel.albums.enumerated() {
            let headerViewModel = CollapsableSectionHeaderViewModel(sectionIdentifier: index, albumViewModel: albumViewModel, collapsedState: .closed)
            headerViewModels.append(headerViewModel)
        }
    }
} 

// MARK: UITableViewDelegate

extension PostDetailDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let collapsableHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CollapsableSectionHeaderView.identifier) as? CollapsableSectionHeaderView else {
            clog("Unable to typecast reusable CollapsableSectionHeaderView in PostDetailDataSource", priority: .error)
            return nil
        }
        
        collapsableHeaderView.configure(viewModel: self.headerViewModels[section])
        collapsableHeaderView.delegate = self
        collapsableHeaderView.sizeToFit()
        return collapsableHeaderView
    }
}

// MARK: UITableViewDataSource

extension PostDetailDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.albums.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let headerViewModel = self.headerViewModels[section]
        return headerViewModel.collapsedState == .closed ? 0 : headerViewModel.albumViewModel.photoViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PhotoTableViewCell.identifier, for: indexPath) as? PhotoTableViewCell else {
            clog("Unable to typecast reusable PhotoTableViewCell in PostDetailDataSource", priority: .error)
            fatalError()
        }
        
        let photoViewModel = self.headerViewModels[indexPath.section].albumViewModel.photoViewModels[indexPath.row]
        cell.configure(viewModel: photoViewModel)
        cell.selectionStyle = .none
        return cell 
    }
}

// MARK: CollapsableSectionHeaderViewDelegate

extension PostDetailDataSource: CollapsableSectionHeaderViewDelegate {
    func collapsableSectionHeaderView(_ headerView: CollapsableSectionHeaderView, at section: Int, didUpdateTo state: CollapsableSectionHeaderView.CollapsedState) {
        var headerViewModel = self.headerViewModels[section]
        headerViewModel.update(to: state)
        self.headerViewModels[section] = headerViewModel
        self.delegate?.postDetailDataSource(self, sectionNeedsUpdatingAt: section)
    } 
}
