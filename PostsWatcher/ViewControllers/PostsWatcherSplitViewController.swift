//
//  PostsWatcherSplitViewController.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

final class PostsWatcherSplitViewController: UISplitViewController {
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    private lazy var overlay: UIView = {
        let overlay = UIView(frame: self.view.frame)
        overlay.backgroundColor = .gray
        overlay.alpha = 0.4
        return overlay
    }()
        
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func presentLoadingLayer() {
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.centerInSuperview()
        self.activityIndicator.startAnimating()
        self.view.addSubview(self.overlay)
    }   
    
    func removeLoadingLayer() {
        UIView.animate(withDuration: 0.33, animations: { 
            self.overlay.alpha = 0
            self.activityIndicator.alpha = 0
        }) { (isFinished) in
            self.overlay.removeFromSuperview()
            self.activityIndicator.stopAnimating()
        }
    }
}
