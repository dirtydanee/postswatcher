//
//  UIViewExtension.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 04.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

extension UIView {
    
    var centerX: CGFloat {
        get { return self.center.x }
        set { self.center.x = newValue } 
    }
    
    var centerY: CGFloat {
        get { return self.center.y }
        set { self.center.y = newValue }
    }
    
    var origin: CGPoint {
        get { return self.frame.origin }
        set { self.frame.origin = newValue }
    }
    
    var width: CGFloat {
        get { return self.frame.size.width }
        set { self.frame.size.width = newValue }
    }
    
    var height: CGFloat {
        get { return self.frame.size.height }
        set { self.frame.size.height = newValue }
    }
    
    public func centerInSuperview() {
        guard let superview = self.superview else {
            return
        }
        self.origin = CGPoint(x: floor((superview.width - self.width) / 2),
                              y: floor((superview.height - self.height) / 2))
    }
    
    public func resize(for width: CGFloat, height: CGFloat = CGFloat.greatestFiniteMagnitude) {
        let resizedSize = self.sizeThatFits(CGSize(width: width, height: height))
        let height = min(height, resizedSize.height)
        self.frame.size = CGSize(width: resizedSize.width, height: height)
    }
}
