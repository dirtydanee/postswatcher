//
//  PhotoTransformer.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import SwiftyJSON

final class PhotoTransformer: ContentTransformer {
    
    var identifier: ContentIdentifier {
        return .photos
    }
    
    let store: PhotosStore
    let managedContext: NSManagedObjectContext
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        self.store = try PhotosStore(managedContext: managedContext)
    }
    
    // MARK: - Public API
    
    func transform(json: JSON) throws {
        let photoId = json["id"].int64Value
        let photo: Photo = try self.store.fetchOrCreateEntity(with: photoId)
        photo.id = photoId
        photo.title = json["title"].stringValue
        photo.url = json["url"].stringValue
        photo.thumbnailUrl = json["thumbnailUrl"].stringValue
        photo.albumId = json["albumId"].int64Value
    }
}
