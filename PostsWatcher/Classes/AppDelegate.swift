//
//  AppDelegate.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var applicationPresenter: ApplicationPresenter? 
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        do {
            
            let applicationPresenter = try ApplicationPresenter()
            applicationPresenter.startUserInterface()
            applicationPresenter.synronizeApplicationContent()
            self.applicationPresenter = applicationPresenter
            
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = applicationPresenter.viewController
            window.makeKeyAndVisible()
            self.window = window
        
        } catch let e {
            clog("Error while creating ApplicationSupervisor. ERROR: \(e.localizedDescription)", priority: .error)
        }
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.applicationPresenter?.saveCoreDataContext()
    }
}

