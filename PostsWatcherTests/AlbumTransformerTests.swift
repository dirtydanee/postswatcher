//
//  AlbumTransformerTests.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 06.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import PostsWatcher

class AlbumTransformerTests: CoreDataTestCase {
    
    func testTransformation() throws {
        
        let transformer = try AlbumTransformer(managedContext: self.managedContext)
        let userData = try Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "Album", ofType:"json")!))
        let json = JSON(data: userData, options: .allowFragments, error: nil)
        try transformer.transform(json: json)
        
        let album: Album = try transformer.store.fetchOrCreateEntity(with: json["id"].int64Value)
        XCTAssertEqual(album.id, json["id"].int64!)
        XCTAssertEqual(album.userId, json["userId"].int64!)
        XCTAssertEqual(album.title, json["title"].string!)
    }
}
