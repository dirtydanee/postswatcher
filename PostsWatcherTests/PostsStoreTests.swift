//
//  PostsStoreTests.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 08.09.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import XCTest
import CoreData
@testable import PostsWatcher

class PostsStoreTests: CoreDataTestCase {
    
    var store: PostsStore!
    
    override func setUp() {
        super.setUp()
        self.store = try? PostsStore(managedContext: self.managedContext)
    }
    
    override func tearDown() {
        self.store = nil
        super.tearDown()
    }
    
    func testfetchPostWithExistingIdentifier() throws {
        let identifier = Int64(1000)
        try createEntity(with: identifier)
        XCTAssertNotNil(try self.store.post(with: identifier))
    }
    
    func testfetchPostWithNonExistingIdentifier() throws {
        XCTAssertNil(try self.store.post(with: Int64(-1)))
    }
    
    func testPostsForExistingUser() throws {
        let identifier = Int64(1)
        let post = try createEntity(with: identifier)
        post.userId = identifier
        
        let userStore: UsersStore = try UsersStore(managedContext: self.managedContext)
        let _: User = try userStore.fetchOrCreateEntity(with: identifier)
        let posts = try self.store.posts(forUser: identifier)
        XCTAssertEqual(posts.count, 1)
        XCTAssertTrue(posts.contains(where: {$0.userId == identifier}))
    }
    
    func testPostsForNonExistingUser() throws {
        let posts = try self.store.posts(forUser: Int64(-1))
        XCTAssertTrue(posts.isEmpty)
    }
    
    func testAllPosts() throws {
        let id1 = Int64(1)
        let id2 = Int64(2)
        let id3 = Int64(3)
        let id4 = Int64(4)
        
        try createEntity(with: id1)
        try createEntity(with: id2)
        try createEntity(with: id3)
        try createEntity(with: id4)
        
        let posts = try self.store.allPosts()
        XCTAssertEqual(posts.count, 4)
        XCTAssertTrue(posts.contains(where: {$0.id == id1}))
        XCTAssertTrue(posts.contains(where: {$0.id == id2}))
        XCTAssertTrue(posts.contains(where: {$0.id == id3}))
        XCTAssertTrue(posts.contains(where: {$0.id == id4}))
    }
    
    func testPostSearch() throws {
        let id1 = Int64(1)
        let post1 = try createEntity(with: id1)
        post1.title = "whatever"
         
        let id2 = Int64(2)
        let post2 = try createEntity(with: id2)
        let userStore: UsersStore = try UsersStore(managedContext: self.managedContext)
        let user: User = try userStore.fetchOrCreateEntity(with: Int64(4))
        user.email = "some"
        user.posts = Set([post2])
        
        var posts = try self.store.posts(containing: "w")
        XCTAssertEqual(posts.count, 1)
        XCTAssertTrue(posts.contains(where: {$0.id == id1}))
        
        posts = try self.store.posts(containing: "s")
        XCTAssertEqual(posts.count, 1)
        XCTAssertTrue(posts.contains(where: {$0.id == id2}))
    }
    
    func testPostDelete() throws {
        let post = try createEntity(with: Int64(1))
        XCTAssertEqual(try self.store.allPosts().count, 1)
        
        try self.store.delete(post)
        XCTAssertEqual(try self.store.allPosts().count, 0)
    }
    
    @discardableResult
    private func createEntity(with identifier: Int64) throws -> Post {
        guard let post = NSManagedObject(entity: self.store.entity, insertInto: self.managedContext) as? Post else {
            throw CoreDataTestError.unableToCreateEntity(identifier: self.store.contentIdentifier.rawValue)
        }
        post.id = identifier
        return post
    }  

}
