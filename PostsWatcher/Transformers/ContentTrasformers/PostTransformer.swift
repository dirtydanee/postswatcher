//
//  PostTransformer.swift
//  PostsWatcher
//
//  Created by Daniel.Metzing on 30.08.17.
//  Copyright © 2017 dirtylabs. All rights reserved.
//

import CoreData
import SwiftyJSON

final class PostTransformer: ContentTransformer {
    
    var identifier: ContentIdentifier {
        return .posts
    }
    
    let store: PostsStore
    let managedContext: NSManagedObjectContext
    
    init(managedContext: NSManagedObjectContext) throws {
        self.managedContext = managedContext
        self.store = try PostsStore(managedContext: managedContext)
    }

    // MARK: - Public API
    
    func transform(json: JSON) throws {
        let postId = json["id"].int64Value
        let post: Post = try self.store.fetchOrCreateEntity(with: postId)
        post.id = postId
        post.body = json["body"].stringValue
        post.title = json["title"].stringValue
        post.userId = json["userId"].int64Value
    }
}
